var superagent = require("superagent");
var imgdata = require('./data.js');
var request = require('request');
var fs = require('fs');
var async = require('async');
var users = [];
var i = 0;

var furl = "http://api.eyekey.com/face/Check/checking?app_id=f89ae61fd63d4a63842277e9144a6bd2&app_key=af1cd33549c54b27ae24aeb041865da2&url=";

function face(item) {
    superagent.post(furl + item.img).set("dataType", "json").end(function(err, response) {
        if(err) {
            console.log(err);
        } else {
            var data = JSON.parse(response.text);
            try {
                if(data.face[0].attribute.gender == 'Female') {
                    console.log("正在下载~"+item.img);
                    downLoadImg(item)
                }
            } catch(e) {
                console.log("验证失败~"+item.name);
            }
        }
    })
}

function startDownLoad(imgdata){
//  imgdata.forEach(function(item){
//      face(item);
//  })
    async.eachLimit(imgdata, 3, function (item, callback) {
        face(item);
        callback();
    }, function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log('finish fetch all');
        }
    });
    
}

function downLoadImg(image) {
    request.head(image.img, function(err, res, body) {
        if(err) {
            console.log(err);
        }
    });
    request(image.img).pipe(fs.createWriteStream('./image/' + image.name+Date.now() + '.jpg'));
}

startDownLoad(imgdata);


