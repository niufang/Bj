var superagent = require("superagent");
var https = require('https');
var request = require('request');
var fs = require('fs');
var users = [];
var i = 0;

var url = "https://www.zhihu.com/api/v4/members/ni-ba-tie-ren/followees?include=data%5B*%5D.answer_count%2Carticles_count%2Cgender%2Cfollower_count%2Cis_followed%2Cis_following%2Cbadge%5B%3F(type%3Dbest_answerer)%5D.topics&offset=0&limit=20";
var zurl = "https://www.zhihu.com/api/v4/members/aaaaa/followees?include=data%5B*%5D.answer_count%2Carticles_count%2Cgender%2Cfollower_count%2Cis_followed%2Cis_following%2Cbadge%5B%3F(type%3Dbest_answerer)%5D.topics&offset=0&limit=20";

var browserMsg1 = {
    "authorization": "Bearer Mi4wQUJETTJlanBOQWtBVU1LcDk2QVlDeGNBQUFCaEFsVk5neWJhV0FCWXJueEs2bjJwcUYwdzBTdmVpYmxVS1hmWkl3|1488100019|625fed8bf4dee0970f731c7ecfba9f1886ca4a5b"
};

function getDataList(url) {
    superagent.get(url).set(browserMsg1).end(function(err, response) {
        if(err) {
            console.log(err);
        } else {
            var d = response.body.data;
            d.forEach(function(item) {
                if(item.gender == 0){
                    console.log("正在抓取"+item.avatar_url)
                    users.push({
                        "name": item.name,
                        "img": item.avatar_url.replace("_is",""),
                        "url_token":item.url_token
                    })
                }
            })
            if(response.body.paging.is_end) {
                if(users.length >= 1000) {
                    console.log('抓取完成');
                    downLoadContent(JSON.stringify(users));
                    return;
                }else{
                    console.log("第"+i+"个用户的数据~~~~~~");
                    getDataList(zurl.replace("aaaaa",users[i].url_token))
                    i++;
                }
                
            } else {
                if(users.length >= 1000) {
                    downLoadContent(JSON.stringify(users));
                    return;
                }
                getDataList(response.body.paging.next);
            }

        }
    })
}

function downLoadContent(cont) {
    fs.appendFile('./' + 'data.js', "module.exports ="+cont, 'utf-8', function(err) {
        if(err) {
            console.log(err);
        } else
            console.log('success');
    });
}

function downLoadImg(image) {
    request.head(image.img, function(err, res, body) {
        if(err) {
            console.log(err);
        }
    });
    request(image.img).pipe(fs.createWriteStream('./image/' + image.name+Date.now() + '.jpg'));
}

getDataList(url);
